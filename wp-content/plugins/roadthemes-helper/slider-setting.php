<?php 

if( ! function_exists( 'road_get_slider_setting' ) ) {
	function road_get_slider_setting() {
		return array(
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Style', 'makali' ),
				'param_name'  => 'style',
				'value'       => array(
					__( 'Grid view', 'makali' )     => 'product-grid',
					__( 'List view', 'makali' )     => 'product-list',
					__( 'Countdown', 'makali' )     => 'product-countdown',
				),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => __( 'Enable slider', 'makali' ),
				'description' => __( 'If slider is enabled, the "column" ins General group is the number of rows ', 'makali' ),
				'param_name'  => 'enable_slider',
				'value'       => true,
				'save_always' => true, 
				'group'       => __( 'Slider Options', 'makali' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: over 1201px)', 'makali' ),
				'param_name' => 'items_1201up',
				'group'      => __( 'Slider Options', 'makali' ),
				'value'      => esc_html__( '4', 'makali' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: 993px - 1200px)', 'makali' ),
				'param_name' => 'items_993_1200',
				'group'      => __( 'Slider Options', 'makali' ),
				'value'      => esc_html__( '4', 'makali' ),
			), 
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: 769px - 992px)', 'makali' ),
				'param_name' => 'items_769_992',
				'group'      => __( 'Slider Options', 'makali' ),
				'value'      => esc_html__( '3', 'makali' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: 641px - 768px)', 'makali' ),
				'param_name' => 'items_641_768',
				'group'      => __( 'Slider Options', 'makali' ),
				'value'      => esc_html__( '2', 'makali' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: 481px - 640px)', 'makali' ),
				'param_name' => 'items_481_640',
				'group'      => __( 'Slider Options', 'makali' ),
				'value'      => esc_html__( '2', 'makali' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: under 480px)', 'makali' ),
				'param_name' => 'items_0_480',
				'group'      => __( 'Slider Options', 'makali' ),
				'value'      => esc_html__( '1', 'makali' ),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Navigation', 'makali' ),
				'param_name'  => 'navigation',
				'save_always' => true,
				'group'       => __( 'Slider Options', 'makali' ),
				'value'       => array(
					__( 'Yes', 'makali' ) => true,
					__( 'No', 'makali' )  => false,
				),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Pagination', 'makali' ),
				'param_name'  => 'pagination',
				'save_always' => true,
				'group'       => __( 'Slider Options', 'makali' ),
				'value'       => array(
					__( 'No', 'makali' )  => false,
					__( 'Yes', 'makali' ) => true,
				),
			),
			array(
				'type'        => 'textfield',
				'heading'     => __( 'Item Margin (unit:pixel)', 'makali' ),
				'param_name'  => 'item_margin',
				'value'       => 30,
				'save_always' => true,
				'group'       => __( 'Slider Options', 'makali' ),
			),
			array(
				'type'        => 'textfield',
				'heading'     => __( 'Slider speed number (unit: second)', 'makali' ),
				'param_name'  => 'speed',
				'value'       => '500',
				'save_always' => true,
				'group'       => __( 'Slider Options', 'makali' ),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => __( 'Slider loop', 'makali' ),
				'param_name'  => 'loop',
				'value'       => true,
				'group'       => __( 'Slider Options', 'makali' ),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => __( 'Slider Auto', 'makali' ),
				'param_name'  => 'auto',
				'value'       => true,
				'group'       => __( 'Slider Options', 'makali' ),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Navigation style', 'makali' ),
				'param_name'  => 'navigation_style',
				'group'       => __( 'Slider Options', 'makali' ),
				'value'       => array(
					'Navigation center horizontal'	=> 'navigation-style1',
					'Navigation top-right'	        => 'navigation-style2',
				),
			),
		);
	}
}