/* Product Options Js */
jQuery(document).ready(function(){

	jQuery('.sale_products.roadthemes-slider').each(function(){ 
		var items_1201up    = parseInt(jQuery(this).attr('data-1201up'));
		var items_993_1200  = parseInt(jQuery(this).attr('data-993-1200'));
		var items_769_992   = parseInt(jQuery(this).attr('data-769-992'));
		var items_641_768   = parseInt(jQuery(this).attr('data-641-768'));
		var items_481_640   = parseInt(jQuery(this).attr('data-481-640'));
		var items_0_480     = parseInt(jQuery(this).attr('data-0-480'));
		var navigation      = true; 
		if (parseInt(jQuery(this).attr('data-navigation'))!==1)  {navigation = false} ;
		var pagination      = false; 
		if (parseInt(jQuery(this).attr('data-pagination'))==1)  {pagination = true} ;
		var item_margin     = parseInt(jQuery(this).attr('data-margin'));
		var auto            = false; 
		if (parseInt(jQuery(this).attr('data-auto'))==1)  {auto = true} ;
		var loop            = false; 
		if (parseInt(jQuery(this).attr('data-loop'))==1)  {loop = true} ;
		var speed           = parseInt(jQuery(this).attr('data-speed'));
		var owljt           = jQuery(this).find('.shop-products').addClass('owl-carousel owl-theme').owlCarousel({ 
			nav            : navigation, 
			dots           : pagination,
			margin         : item_margin,
			loop           : loop,
			autoplay       : auto,
			smartSpeed     : speed,
			addClassActive : false,
			responsiveClass: true,
			responsive     : {
				0: {
					items: items_0_480,
				},
				481: {
					items: items_481_640,
				},
				641: {
					items: items_641_768,
				},
				769: { 
					items: items_769_992,
				},
				993: { 
					items: items_993_1200,
				}, 
				1201: {
					items: items_1201up,
				},
			}
		});
	});
	jQuery('.list_products.roadthemes-slider').each(function(){ 
		var items_1201up    = parseInt(jQuery(this).attr('data-1201up'));
		var items_993_1200  = parseInt(jQuery(this).attr('data-993-1200'));
		var items_769_992   = parseInt(jQuery(this).attr('data-769-992'));
		var items_641_768   = parseInt(jQuery(this).attr('data-641-768'));
		var items_481_640   = parseInt(jQuery(this).attr('data-481-640'));
		var items_0_480     = parseInt(jQuery(this).attr('data-0-480'));
		var navigation      = true; 
		if (parseInt(jQuery(this).attr('data-navigation'))!==1)  {navigation = false} ;
		var pagination      = false; 
		if (parseInt(jQuery(this).attr('data-pagination'))==1)  {pagination = true} ;
		var item_margin     = parseInt(jQuery(this).attr('data-margin'));
		var auto            = false; 
		if (parseInt(jQuery(this).attr('data-auto'))==1)  {auto = true} ;
		var loop            = false; 
		if (parseInt(jQuery(this).attr('data-loop'))==1)  {loop = true} ;
		var speed           = parseInt(jQuery(this).attr('data-speed'));
		var owljt           = jQuery(this).find('.shop-products').addClass('owl-carousel owl-theme').owlCarousel({ 
			nav            : navigation, 
			dots           : pagination,
			margin         : item_margin,
			loop           : loop,
			autoplay       : auto,
			smartSpeed     : speed,
			addClassActive : false,
			responsiveClass: true,
			responsive     : {
				0: {
					items: items_0_480,
				},
				481: {
					items: items_481_640,
				},
				641: {
					items: items_641_768,
				},
				769: { 
					items: items_769_992,
				},
				993: { 
					items: items_993_1200,
				}, 
				1201: {
					items: items_1201up,
				},
			}
		});
	});
	
	jQuery('.best_selling_products.roadthemes-slider').each(function(){ 
		var items_1201up    = parseInt(jQuery(this).attr('data-1201up'));
		var items_993_1200  = parseInt(jQuery(this).attr('data-993-1200'));
		var items_769_992   = parseInt(jQuery(this).attr('data-769-992'));
		var items_641_768   = parseInt(jQuery(this).attr('data-641-768'));
		var items_481_640   = parseInt(jQuery(this).attr('data-481-640'));
		var items_0_480     = parseInt(jQuery(this).attr('data-0-480'));
		var navigation      = true; 
		if (parseInt(jQuery(this).attr('data-navigation'))!==1)  {navigation = false} ;
		var pagination      = false; 
		if (parseInt(jQuery(this).attr('data-pagination'))==1)  {pagination = true} ;
		var item_margin     = parseInt(jQuery(this).attr('data-margin'));
		var auto            = false; 
		if (parseInt(jQuery(this).attr('data-auto'))==1)  {auto = true} ;
		var loop            = false; 
		if (parseInt(jQuery(this).attr('data-loop'))==1)  {loop = true} ;
		var speed           = parseInt(jQuery(this).attr('data-speed'));
		var owljt           = jQuery(this).find('.shop-products').addClass('owl-carousel owl-theme').owlCarousel({ 
			nav            : navigation, 
			dots           : pagination,
			margin         : item_margin,
			loop           : loop,
			autoplay       : auto,
			smartSpeed     : speed,
			addClassActive : false,
			responsiveClass: true,
			responsive     : {
				0: {
					items: items_0_480,
				},
				481: {
					items: items_481_640,
				},
				641: {
					items: items_641_768,
				},
				769: { 
					items: items_769_992,
				},
				993: { 
					items: items_993_1200,
				}, 
				1201: {
					items: items_1201up,
				}, 
			}
		});
	});

	jQuery('.recent_products.roadthemes-slider').each(function(){ 
		var items_1201up    = parseInt(jQuery(this).attr('data-1201up'));
		var items_993_1200  = parseInt(jQuery(this).attr('data-993-1200'));
		var items_769_992   = parseInt(jQuery(this).attr('data-769-992'));
		var items_641_768   = parseInt(jQuery(this).attr('data-641-768'));
		var items_481_640   = parseInt(jQuery(this).attr('data-481-640'));
		var items_0_480     = parseInt(jQuery(this).attr('data-0-480'));
		var navigation      = true; 
		if (parseInt(jQuery(this).attr('data-navigation'))!==1)  {navigation = false} ;
		var pagination      = false; 
		if (parseInt(jQuery(this).attr('data-pagination'))==1)  {pagination = true} ;
		var item_margin     = parseInt(jQuery(this).attr('data-margin'));
		var auto            = false; 
		if (parseInt(jQuery(this).attr('data-auto'))==1)  {auto = true} ;
		var loop            = false; 
		if (parseInt(jQuery(this).attr('data-loop'))==1)  {loop = true} ;
		var speed           = parseInt(jQuery(this).attr('data-speed'));
		var owljt           = jQuery(this).find('.shop-products').addClass('owl-carousel owl-theme').owlCarousel({ 
			nav            : navigation, 
			dots           : pagination,
			margin         : item_margin,
			loop           : loop,
			autoplay       : auto,
			smartSpeed     : speed,
			addClassActive : false,
			responsiveClass: true,
			responsive     : {
				0: {
					items: items_0_480,
				},
				481: {
					items: items_481_640,
				},
				641: {
					items: items_641_768,
				},
				769: { 
					items: items_769_992,
				},
				993: { 
					items: items_993_1200,
				}, 
				1201: {
					items: items_1201up,
				}, 
			}
		});
	});

	jQuery('.toprated_products.roadthemes-slider').each(function(){ 
		var items_1201up    = parseInt(jQuery(this).attr('data-1201up'));
		var items_993_1200  = parseInt(jQuery(this).attr('data-993-1200'));
		var items_769_992   = parseInt(jQuery(this).attr('data-769-992'));
		var items_641_768   = parseInt(jQuery(this).attr('data-641-768'));
		var items_481_640   = parseInt(jQuery(this).attr('data-481-640'));
		var items_0_480     = parseInt(jQuery(this).attr('data-0-480'));
		var navigation      = true; 
		if (parseInt(jQuery(this).attr('data-navigation'))!==1)  {navigation = false} ;
		var pagination      = false; 
		if (parseInt(jQuery(this).attr('data-pagination'))==1)  {pagination = true} ;
		var item_margin     = parseInt(jQuery(this).attr('data-margin'));
		var auto            = false; 
		if (parseInt(jQuery(this).attr('data-auto'))==1)  {auto = true} ;
		var loop            = false; 
		if (parseInt(jQuery(this).attr('data-loop'))==1)  {loop = true} ;
		var speed           = parseInt(jQuery(this).attr('data-speed'));
		var owljt           = jQuery(this).find('.shop-products').addClass('owl-carousel owl-theme').owlCarousel({ 
			nav            : navigation, 
			dots           : pagination,
			margin         : item_margin,
			loop           : loop,
			autoplay       : auto,
			smartSpeed     : speed,
			addClassActive : false,
			responsiveClass: true,
			responsive     : {
				0: {
					items: items_0_480,
				},
				481: {
					items: items_481_640,
				},
				641: {
					items: items_641_768,
				},
				769: { 
					items: items_769_992,
				},
				993: { 
					items: items_993_1200,
				}, 
				1201: {
					items: items_1201up,
				}, 
			}
		});
	});

	jQuery('.featured_products.roadthemes-slider').each(function(){ 
		var items_1201up    = parseInt(jQuery(this).attr('data-1201up'));
		var items_993_1200  = parseInt(jQuery(this).attr('data-993-1200'));
		var items_769_992   = parseInt(jQuery(this).attr('data-769-992'));
		var items_641_768   = parseInt(jQuery(this).attr('data-641-768'));
		var items_481_640   = parseInt(jQuery(this).attr('data-481-640'));
		var items_0_480     = parseInt(jQuery(this).attr('data-0-480'));
		var navigation      = true; 
		if (parseInt(jQuery(this).attr('data-navigation'))!==1)  {navigation = false} ;
		var pagination      = false; 
		if (parseInt(jQuery(this).attr('data-pagination'))==1)  {pagination = true} ;
		var item_margin     = parseInt(jQuery(this).attr('data-margin'));
		var auto            = false; 
		if (parseInt(jQuery(this).attr('data-auto'))==1)  {auto = true} ;
		var loop            = false; 
		if (parseInt(jQuery(this).attr('data-loop'))==1)  {loop = true} ;
		var speed           = parseInt(jQuery(this).attr('data-speed'));
		var owljt           = jQuery(this).find('.shop-products').addClass('owl-carousel owl-theme').owlCarousel({ 
			nav            : navigation, 
			dots           : pagination,
			margin         : item_margin,
			loop           : loop,
			autoplay       : auto,
			smartSpeed     : speed,
			addClassActive : false,
			responsiveClass: true,
			responsive     : {
				0: {
					items: items_0_480,
				},
				481: {
					items: items_481_640,
				},
				641: {
					items: items_641_768,
				},
				769: { 
					items: items_769_992,
				},
				993: { 
					items: items_993_1200,
				}, 
				1201: {
					items: items_1201up,
				},
			}
		});
	});

	jQuery('.category_products.roadthemes-slider').each(function(){ 
		var items_1201up    = parseInt(jQuery(this).attr('data-1201up'));
		var items_993_1200  = parseInt(jQuery(this).attr('data-993-1200'));
		var items_769_992   = parseInt(jQuery(this).attr('data-769-992'));
		var items_641_768   = parseInt(jQuery(this).attr('data-641-768'));
		var items_481_640   = parseInt(jQuery(this).attr('data-481-640'));
		var items_0_480     = parseInt(jQuery(this).attr('data-0-480'));
		var navigation      = true; 
		if (parseInt(jQuery(this).attr('data-navigation'))!==1)  {navigation = false} ;
		var pagination      = false; 
		if (parseInt(jQuery(this).attr('data-pagination'))==1)  {pagination = true} ;
		var item_margin     = parseInt(jQuery(this).attr('data-margin'));
		var auto            = false; 
		if (parseInt(jQuery(this).attr('data-auto'))==1)  {auto = true} ;
		var loop            = false; 
		if (parseInt(jQuery(this).attr('data-loop'))==1)  {loop = true} ;
		var speed           = parseInt(jQuery(this).attr('data-speed'));
		var owljt           = jQuery(this).find('.shop-products').addClass('owl-carousel owl-theme').owlCarousel({ 
			nav            : navigation, 
			dots           : pagination,
			margin         : item_margin,
			loop           : loop,
			autoplay       : auto,
			smartSpeed     : speed,
			addClassActive : false,
			responsiveClass: true,
			responsive     : {
				0: {
					items: items_0_480,
				},
				481: {
					items: items_481_640,
				},
				641: {
					items: items_641_768,
				},
				769: { 
					items: items_769_992,
				},
				993: { 
					items: items_993_1200,
				}, 
				1201: {
					items: items_1201up,
				},
			}
		});
	});

	jQuery('.categories_products.roadthemes-slider').each(function(){ 
		var items_1201up    = parseInt(jQuery(this).attr('data-1201up'));
		var items_993_1200  = parseInt(jQuery(this).attr('data-993-1200'));
		var items_769_992   = parseInt(jQuery(this).attr('data-769-992'));
		var items_641_768   = parseInt(jQuery(this).attr('data-641-768'));
		var items_481_640   = parseInt(jQuery(this).attr('data-481-640'));
		var items_0_480     = parseInt(jQuery(this).attr('data-0-480'));
		var navigation      = true; 
		if (parseInt(jQuery(this).attr('data-navigation'))!==1)  {navigation = false} ;
		var pagination      = false; 
		if (parseInt(jQuery(this).attr('data-pagination'))==1)  {pagination = true} ;
		var item_margin     = parseInt(jQuery(this).attr('data-margin'));
		var auto            = false; 
		if (parseInt(jQuery(this).attr('data-auto'))==1)  {auto = true} ;
		var loop            = false; 
		if (parseInt(jQuery(this).attr('data-loop'))==1)  {loop = true} ;
		var speed           = parseInt(jQuery(this).attr('data-speed'));
		var owljt           = jQuery(this).find('.shop-products').addClass('owl-carousel owl-theme').owlCarousel({ 
			nav            : navigation, 
			dots           : pagination,
			margin         : item_margin,
			loop           : loop,
			autoplay       : auto,
			smartSpeed     : speed,
			addClassActive : false,
			responsiveClass: true,
			responsive     : {
				0: {
					items: items_0_480,
				},
				481: {
					items: items_481_640,
				},
				641: {
					items: items_641_768,
				},
				769: { 
					items: items_769_992,
				},
				993: { 
					items: items_993_1200,
				}, 
				1201: {
					items: items_1201up,
				},
			}
		});
	});

	jQuery('.categories_products.roadthemes-slider .shop-products').each(function(){ 
		var items_1201up    = parseInt(jQuery(this).attr('data-1201up'));
		var items_993_1200  = parseInt(jQuery(this).attr('data-993-1200'));
		var items_769_992   = parseInt(jQuery(this).attr('data-769-992'));
		var items_641_768   = parseInt(jQuery(this).attr('data-641-768'));
		var items_481_640   = parseInt(jQuery(this).attr('data-481-640'));
		var items_0_480     = parseInt(jQuery(this).attr('data-0-480'));
		var navigation      = true; 
		if (parseInt(jQuery(this).attr('data-navigation'))!==1)  {navigation = false} ;
		var pagination      = false; 
		if (parseInt(jQuery(this).attr('data-pagination'))==1)  {pagination = true} ;
		var item_margin     = parseInt(jQuery(this).attr('data-margin'));
		var auto            = false; 
		if (parseInt(jQuery(this).attr('data-auto'))==1)  {auto = true} ;
		var loop            = false; 
		if (parseInt(jQuery(this).attr('data-loop'))==1)  {loop = true} ;
		var speed           = parseInt(jQuery(this).attr('data-speed'));
		var owljt           = jQuery(this).find('.shop-products').addClass('owl-carousel owl-theme').owlCarousel({ 
			nav            : navigation, 
			dots           : pagination,
			margin         : item_margin,
			loop           : loop,
			autoplay       : auto,
			smartSpeed     : speed,
			addClassActive : false,
			responsiveClass: true,
			responsive     : {
				0: {
					items: items_0_480,
				},
				481: {
					items: items_481_640,
				},
				641: {
					items: items_641_768,
				},
				769: { 
					items: items_769_992,
				},
				993: { 
					items: items_993_1200,
				}, 
				1201: {
					items: items_1201up,
				},
			}
		});
	});
	
}); 